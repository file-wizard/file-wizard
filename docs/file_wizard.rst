file\_wizard package
====================

Submodules
----------

file\_wizard.file\_manager module
---------------------------------

.. automodule:: file_wizard.file_manager
   :members:
   :undoc-members:
   :show-inheritance:

file\_wizard.logger module
--------------------------

.. automodule:: file_wizard.logger
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: file_wizard
   :members:
   :undoc-members:
   :show-inheritance:
