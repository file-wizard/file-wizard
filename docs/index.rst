.. PdfCC documentation master file, created by
   sphinx-quickstart on Sat Nov 21 00:16:24 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to File-Wizard's documentation!
=======================================

.. toctree::
   :maxdepth: 5
   :caption: Contents:
   
   readme
   File-Wizard <modules>



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
