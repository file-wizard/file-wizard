# Contributing

The following rules need to be followed by those wanting to contribute:
1. Make sure no junk files are included or tracked by the git repositroy. Junk files include IDE based config files and other files not part of the repository that may have been personally used by the contributor.
2. Make sure to follow the coding standards specified by pycodestyle. Make sure all the variables and functions are in camelCase. Assistance in this regard is provided by the .pylintrc file that can be used to verify your code. You can also use pylint extensions in your favourite IDE to provide realtime assistance.
3. Contribution can be done by forking the repository, making you additions and changes, and then sending a pull request to the maintainers. Make sure to include the details of what new features or bug fixes that have been added, so that it is easier for the maintainers to evaluate the PR.
4. Make sure to write proper tests for any changes that have been made. This can also include editing existing tests to fit the changes made to an existing feature.
For further information, contact the maintainers.