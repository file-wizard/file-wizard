"""Module contains functions for testing the file_manager module.
"""
from os import path
import sys
import hashlib
from file_wizard.file_manager import FileManager


def testFileManagerMkdtmp():
    """Test for checking whether FileManager.mkdtemp works
    """
    fileManager: FileManager = FileManager()
    fileName = fileManager.mkdtemp(suffix="file-wizard-test-")
    assert path.exists(fileName) is True


def testFileManagerRmdtmpAutoDelete():
    """Test for checking whether FileManager.rmdtemp works
    """
    fileManager: FileManager = FileManager()
    fileName = fileManager.mkdtemp(prefix="file-wizard-test-")
    assert path.exists(fileName) is True
    fileManager.rmdtemp()
    assert path.exists(fileName) is False


def testFileManagerRmdtmp():
    """Test for checking whether FileManager.rmdtemp autodelete set to \
        false works.
    """
    fileManager: FileManager = FileManager()
    fileName = fileManager.mkdtemp(prefix="file-wizard-test-",
                                   autoDelete=False)
    assert path.exists(fileName) is True
    fileManager.rmdtemp()
    assert path.exists(fileName) is True


def testFileManagerDoubleHash():
    """Test for checking the hash values generated
    """
    fMan = FileManager()
    tfile = path.join(sys.path[0], "data/hash_test")
    hashval = fMan.doubleHash(tfile)

    assert hashval["status"] is True

    md5 = hashlib.md5()
    sha1 = hashlib.sha1()
    with open(tfile, "rb") as testHandler:
        while True:
            data = testHandler.read(fMan.bufferSize)
            if not data:
                break
            md5.update(data)
            sha1.update(data)

    assert (md5.hexdigest() == hashval["md5"]) is True
    assert (sha1.hexdigest() == hashval["sha1"]) is True


def testDoubleHashMissing():
    """Test for checking DoubleHash file missing
    """
    fMan = FileManager()
    tfile = path.join(sys.path[0], "data/missing.pdf")
    hashval = fMan.doubleHash(tfile)
    assert hashval["status"] is False


def testFMBufferSize():
    """Test for checking DoubleHash file missing
    """
    fMan = FileManager()
    fMan.bufferSize = -20
    assert fMan.bufferSize != -20
    fMan.bufferSize = 20
    assert fMan.bufferSize == 20
