"""Module contains functions for testing the logger module.
"""
import logging
from file_wizard import logger


def testLoggerConsoleDisable(capfd):
    """Function checks whether the console logging is working.
    """
    log: logging.Logger = logger.initLogger(verbose=False)
    message: str = "Hello world"
    log.debug(message)
    captured = capfd.readouterr()
    assert message not in captured.err


def testLoggerFile(caplog):
    """Function checks whether the file logging is working.
    """
    log: logging.Logger = logger.initLogger()
    message: str = "Hello world"
    log.setLevel(logging.DEBUG)
    log.debug(message)
    txt: str = caplog.text
    assert message in txt


def testLoggerRemoveHandler():
    """Checks whether the removeHandler() is working
    """
    log = logging.getLogger("FileWizardTesting")
    nullHandler = logging.NullHandler()
    log.addHandler(nullHandler)
    assert len(log.handlers) == 1
    logger.removeHandler(log, logging.NullHandler)
    assert len(log.handlers) == 0


def testLoggerLogFileDisable():
    """Function checks whether the logfile disable is working.
    """
    log: logging.Logger = logger.initLogger()
    assert logger.isHandlerPresent(log, logging.FileHandler) is True
    log1: logging.Logger = logger.initLogger(logfile=False)
    assert logger.isHandlerPresent(log1, logging.FileHandler) is False
    assert logger.isHandlerPresent(log, logging.FileHandler) is False
